""" Library that allows validating specific sales transactions functionality of JSON, XML files and associated arrays
represented as python built-in dictionaries. TransactionsValidator object incapsulates all validation logic,
to execute validation functionality, you need to create TransactionsValidator object and run_validation method.
TransactionsValidator class supports different validation scenarios which depend on parameters passed:
 - Scenario #1: specific file (XML or JSON) if it is passed as transaction_file_path='path/to/file'.
 - Scenario #2: python dictionary if it is passed as a associated_array={key: value....}
 - Scenario #3: all files from specific folder if this folder is passed as transactions_folder_path ='path/to/folder'
            Is a default scenario when no params passed in by using default folder as an environment variable or hardcoded path.
Only one scenario of validation can work for each object created by TransactionsValidator class based params defined.
"""


import logging
import json
import os

import xmltodict


TRANSACTIONS_DEFAULT_PATH = os.getenv('TRANSACTIONS_PATH', './transactions/trans/trans/')


class TransactionsValidator:

    def __init__(
            self, transactions_folder_path=TRANSACTIONS_DEFAULT_PATH, transaction_file_path=None, associated_array=None
    ):
        self.transactions_folder_path = transactions_folder_path
        self.transaction_file_path = transaction_file_path
        self.associated_array = associated_array or {}
        self.logger = logging.getLogger(str(self))

    @staticmethod
    def get_taxes_data(data):
        # TODO: hard logic for xml should be reviewed
        # blind 1st element slicing implemented below because it's unclear why taxes data is an array of the same objects
        data = data[0] if 'element' not in data else data['element'][0]
        rate, inclusion_type = float(data['rate']), data['inclusion_type']
        amount = float(data['applied_money']['amount'])
        return rate, inclusion_type, amount

    @staticmethod
    def get_sale_details(data):
        # dict obj got from XML file includes XML specific element key, so needs custom parsing
        # TODO: hard logic for xml should be reviewed
        data = data[0] if 'element' not in data else data['element']
        qty = data['quantity']
        item_price = data['single_quantity_money']['amount']
        total_price = data['total_money']['amount']
        gross_price = data['gross_sales_money']['amount']
        return int(qty), float(item_price), float(total_price), float(gross_price)

    def validate_trans_taxes_data(self, trans_data: dict) -> None:
        """ Validate if sale transactions taxes functionality meets requirements, mainly for taxes amount, inclusivity
        and rate, sale totals, qty and price calculations.
        :rtype: None. But in case of invalidation this method should raise ValueError with specific message
        """
        required_tax_rate, required_inclusive_type, taxes_amount = self.get_taxes_data(trans_data['taxes'])
        qty, item_price, total_price, gross_price = self.get_sale_details(trans_data['itemization'])
        actual_sales_tax_rate = round(item_price / gross_price - 1, 2)
        # validate if taxes rate has been properly applied on the sale order/transaction
        if actual_sales_tax_rate != required_tax_rate:
            raise ValueError('Taxes amount of transactions does not meet the requirements. '
                             f'Required tax rate is {required_tax_rate} but actual trans rate is {actual_sales_tax_rate}')
        # validate the taxes inclusivity
        if required_inclusive_type == 'INCLUSIVE' and total_price != gross_price*qty + taxes_amount:
            raise ValueError('Sale Taxes have been applied incorrectly. '
                             f'Expected Total price: {gross_price*qty}, but actual price: {total_price}.')
        # validate if total price of sale order/transaction been properly calculated
        if total_price != qty * item_price:
            raise ValueError(f'Sale transaction Total price is incorrect. '
                             f'Expected amount: {qty * item_price}, but actual {total_price}.')

    @staticmethod
    def get_data_from_file(file_path: str) -> dict:
        """ Get data from file and covert it to a dict. Allowed file formats are XML and JSON, it raises ValueErrror
        for file of other formats. """
        if not os.path.isfile(file_path):
            raise FileNotFoundError(f'File path {file_path} is invalid because file does not exist.')
        if file_path.endswith('.xml'):
            with open(file_path) as f:
                # need to apply json decoding to make xml converting logic datatype consistent with other places
                # so that return built-it dict obj because xmltodict.parse() returns OrderedDict obj with OrderedDicts
                stringified_obj = json.dumps(xmltodict.parse(f.read()))
                return json.loads(stringified_obj)['root']
        elif file_path.endswith('.json'):
            with open(file_path) as f:
                return json.loads(f.read())
        else:
            raise ValueError(
                f'Invalid format of {file_path.split("/")[-1]} transactions file. JSON, XML allowed file formats.')

    def run_validation(self):
        try:
            # scenario #1: do validation of specific file only if it has been passed in (it is off/None by default)
            if self.transaction_file_path:
                trans_data = self.get_data_from_file(self.transaction_file_path)
                self.validate_trans_taxes_data(trans_data)
                self.logger.info('Validation successful!')
                return True

            # scenario #2: validate associated_array as a dictionary object
            elif self.associated_array and isinstance(self.associated_array, dict):
                self.validate_trans_taxes_data(self.associated_array)
                self.logger.info('Validation successful!')
                return True

            # scenario #3: validate all files of transactions folder (client defined or default one)
            #              in case no specific file passed in (scenario #1 didn't work)
            else:
                if not (os.path.isdir(self.transactions_folder_path) and os.listdir(self.transactions_folder_path)):
                    raise FileNotFoundError('Transactions folder does not exist or have no files inside. '
                                            f'Invalid folder path: {self.transactions_folder_path}.')
                # iterate through transactions folder and validate each file in it
                for file in os.listdir(self.transactions_folder_path):
                    trans_data = self.get_data_from_file(f'{self.transactions_folder_path}/{file}')
                    self.validate_trans_taxes_data(trans_data)
                    print(f'Validation of {file} succeeded')
                    self.logger.info(f'Validation of {file} succeeded')
                print(f'Validation finished successfully!')
                self.logger.info('Validation finished successfully!')
                return True

        except (FileNotFoundError, ValueError) as e:
            self.logger.error(e)
            return False
        except KeyError as e:
            self.logger.error('Key/parameter of transactions file is invalid. Error: ', e)
            return False
        except Exception as e:
            self.logger.error('Unsupported error! Error: ', e)
            return False


if __name__ == '__main__':
    transactions_validator = TransactionsValidator()
    transactions_validator.run_validation()
